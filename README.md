# Per far funzionare gli esempi
è necessario aggiungere questa riga nel proprio file hosts

```127.0.0.1 *.docker.localhost```
### Su Windows
- Eseguire Notepad come amministratori
- Aprire `c:\Windows\System32\Drivers\etc\hosts`
- Aggiungere la linea indicata sopra
- Salvare
### Su Mac/Linux
- `sudo nano /etc/hosts`
- Aggiungere la linea indicata sopra
- Salvare
Click File > Save to save your changes.
# Branch Blue Green
Esempio di Blue green Deployment con Traefik

visitare:
- `http://docker.localhost:8081/` per vedere il deployment attualmente attivo
- `http://blue.docker.localhost:8081/` per vedere il blue
- `http://green.docker.localhost:8081/`per vedere il green

Modificando `dynamic/http.routers.docker-localhost.yml` si può switchare la versione in tempo reale


# Branch Canary
Esempio di Canary Deployment con Traefik
    visitare:
- `http://docker.localhost:8081/` refreshare molteplici volte per vedere il bilanciamento
- `http://blue.docker.localhost:8081/` per vedere il blue
- `http://green.docker.localhost:8081/`per vedere il green

Modificando `dynamic/http.routers.docker-localhost.yml` si può cambiare il bilanciamento in tempo reale
